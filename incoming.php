<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>unics dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="style.css" rel="stylesheet" />
    <link href="login.css" rel="stylesheet" />
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    


</head>
<body><!-- the wrapper consist of the overall body -->

<div class="wrapper">
    <div class="sidebar" data-color="black" data-image="assets/img/unicssidebar.png">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

        <!-- sidebar element are written here -->
<div class="sidebar-wrapper" style="padding-top: 60PX;">
                    <div class="logo">
                         <a href="incoming.php" class="simple-text"> <img src="assets\img\capture1.png" ></a>

                           
                        </a>
                    </div>


            <ul class="nav">
                
                
                
                <li class="active">
                    <a href="incoming.php">
                        
                        <i> <img src="assets\img\incoming.png"></i>
                        <p>INCOMING</p>
                    </a>
                </li>
                <li>
                    <a href="outgoing.php">
                       
                        <i> <img src="assets\img\outgoing.png"></i>
                        <p>OUTGOING</p>
                    </a>
                </li>
                <li>
                    <a href="history.php">
                        
                        <i> <img src="assets\img\history.png"></i>
                        <p>HISTORY</p>
                    </a>
                </li>

                <li>
                    <a href="ctrswitch.php">
                        
                        <i> <img src="assets\img\ctrswitch.png"></i>
                        <p>CTR SWITCH</p>
                    </a>
                </li>
                <li>
                    <a href="velocity.php">
                        
                        <i> <img src="assets\img\velocityswitch.png"></i>
                        <p>VELOCITY SWITCH</p>
                    </a>
                </li>
                <li >
                            <a href="creditscore.php">
                                
                                <i> <img src="assets\img\creditscore.png"></i>
                                <P>CREDIT SCORE</P>
                            </a>
                        </li>
                        <li >
                            <a href="debtoraccount.php">
                                
                                <i> <img src="assets\img\debtorsaccount.png"></i>
                                <P>DEBTOR ACCOUNTS</P>
                            </a>
                        </li>
                
                
            </ul>
        </div>
    </div>

    <!-- the main-panel consist of the rest of the colomn after the sidebar and it is in the wrapper  -->
<div class="main-panel">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                          <div class="logo"><div class="navbar-brand"><img src="assets\img\UNICS.png"  style="margin-top: -7px;">
                           </div>
                           </div>
                            
                        </div>
                <div class="collapse navbar-collapse" >
                    
                        <ul class="nav navbar-nav  ">
                        <li>
                           <a href="createaccount1.php"  >
                             <i> <img src="assets\img\navcontroller.png"></i>   <strong>CONTROLLER</strong>
                            </a>
                        </li>
                        
                        <li class="active" >
                            <a href="incoming.php">
                                <i> <img src="assets\img\navcapture.png"></i> <strong>CAPTURE</strong>
                            </a>
                        </li>
                        
                        <li>
                            <form method="post" action="logout.php" id="">
                            <div class="" style="margin-left:280px; padding-top:18px;"  > 
                                    <i style="color: white;">ECOBANK </i>
                                <a href="index.php" title="logout">
                                <img src="assets\img\logouticon.png"></a>
                                    </div>
                                </form>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>


        <!-- the content consist of the panel which is the panel-head, panel-body and panel-footer -->
    

                    <div class="content" >
                    <div class="container-fluid" style="padding-top: 40px;" >
            <!-- You only need this form and the form-labels-on-top.css -->
            <div class="panel panel-default">
              <div class="panel-heading">
              <div class="panel-heading text-center">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1"><h5><strong>INCOMING </strong></h5></marquee> 
              </div>
              </div>
            <div class="panel-body">
            <div class="panel-body">
            <div class="row">
            
            <div class="col-lg-5">
            <form class="form-labels-on-top" method="post" action="#" id="">
                <div class="form-row">
                <h6> depositing for kwame baah<br> <br> 
                AMOUNT : 500.00 <br> <br>
                BRANCH: MADINA BRANCH<br> <br>                
                CURENCY: GHANA CEDIS
                <br><br> 
                </h6>
                
                <label>
                     
                    <span>TRANSACTION CODE</span>
                    <input type="number" min="0" max="13" name="name" placeholder="CODE" class="validate">
                </label>
            </div>
            
            </form>
            </div>

            <div class="col-lg-5">
            <br> <br> <br> <br> <br> <br> <br>
            <div class="form-row">
            
                <label>

                    <span>TELLER PASSWORD</span>
                    <input type="password" min="0" max="13" name="name" placeholder="" class="validate">
                </label>
                <div class="button pull-right">
<button type="submit" class="btn btn-sm btn-primary">DONE</button>
</div> 
            </div>
                

                
            </div>
            
                

            </div>
            <br>
            
            <hr>

            <div class="row">
            <div class="col-lg-5">
                <div class="form-row">
                <h6> depositing for kwame baah<br> <br> 
                AMOUNT : 500.00 <br> <br>
                BRANCH: MADINA BRANCH<br> <br>                
                CURENCY: GHANA CEDIS
                <br><br> 
                </h6>
                <label>

                    <span>TRANSACTION CODE</span>
                    <input type="number" min="0" max="13" name="name" placeholder="CODE" class="validate">
                </label>
            </div>


            </div>
            <div class="col-lg-5">
            <br> <br> <br> <br> <br> <br> <br>
            <div class="form-row">
            
                <label>

                    <span>TELLER PASSWORD</span>
                    <input type="password" min="0" max="13" name="name" placeholder="" class="validate">
                </label>
                <div class="button pull-right">
<button type="submit" class="btn btn-sm btn-primary">DONE</button>
</div>
            </div>
                

                
            </div>
            
                

            </div>
            <br>
            
            <hr>
            <div class="row">
            <div class="col-lg-5">
                <div class="form-row">
                <h6> depositing for kwame baah<br> <br> 
                AMOUNT : 500.00 <br> <br>
                BRANCH: MADINA BRANCH<br> <br>                
                CURENCY: GHANA CEDIS
                <br><br> 
                </h6>
                <label>

                    <span>TRANSACTION CODE</span>
                    <input type="number" min="0" max="13" name="name" placeholder="CODE" class="validate">
                </label>
            </div>


            </div>
            <div class="col-lg-5">
            <div class="form-row">
            <br> <br> <br> <br> <br> <br> <br>
                <label>

                    <span>TELLER PASSWORD</span>
                    <input type="password" min="0" max="13" name="name" placeholder="" class="validate">
                </label>
                <div class="button pull-right">
<button type="submit" class="btn btn-sm btn-primary">DONE</button>
</div>
            </div>
                

                
            </div>
            
                

            </div>
            <br>
            
            <hr>
            <div class="row">
            <div class="col-lg-5">
                <div class="form-row">
                <h6> depositing for kwame baah<br> <br> 
                AMOUNT : 500.00 <br> <br>
                BRANCH: MADINA BRANCH<br> <br>                
                CURENCY: GHANA CEDIS
                <br><br> 
                </h6>
                <label>

                    <span>TRANSACTION CODE</span>
                    <input type="number" min="0" max="13" name="name" placeholder="CODE" class="validate">
                </label>
            </div>


            </div>
            <div class="col-lg-5">
            <div class="form-row">
            <br> <br> <br> <br> <br> <br> <br>
            
                <label>

                    <span>TELLER PASSWORD</span>
                    <input type="password" min="0" max="13" name="name" placeholder="" class="validate">
                </label>
                <div class="button pull-right">
<button type="submit" class="btn btn-sm btn-primary">DONE</button>
</div>
            </div>
                

                
            </div>
            
                

            </div>
            <br>
            
            <hr>



                

            </div>

            
            
            
            
            </div>


            <div class="panel-footer"> 
            <div class="container">
    
    


      </div>
      </div> 
                

        </div>
        </div>
        </div>
        </div>
        </div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            demo.initChartist();

            $.notify({
                icon: 'pe-7s-gift',
                message: "Welcome to <b>Unics Mains Capture</b> ."

            },{
                type: 'info',
                timer: 4000
            });

        });
    </script>


</html>

