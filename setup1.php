<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>unics dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- <link href="style.css" rel="stylesheet" /> -->
    <!-- <link href="login.css" rel="stylesheet" /> -->
    <link href="setup.css" rel="stylesheet" />
    <link href="createaccount1.css" rel="stylesheet" />

                                
    <link href="assets/css/mdb.min.css" rel="stylesheet">

    

    


</head>
<style>
.body{
    color: white;


} 


</style>

<body><!-- the wrapper consist of the overall body -->

<div class="wrapper">
    

    
        
        <div class="content">




        <!-- You only need this form and the form-labels-on-top.css -->
            <div class="panel panel-default">
            <div class="panel-heading" style="background:url('assets/img/heading.png'); color: white;">
              <div class="panel-heading text-center">
              <div class="logo">
                <a href="#" class="simple-text"> <img src="assets\img\UNICS.png" >
                   
                </a>
            </div>
              <h3><strong>UNICS SETUP</strong></h3> 
              </div>
              </div>
                

            
            
            

            
            
              
            <div class="panel-body" style="background: #a5a5a5;">
            <form method="post" action="setup2.php" id="">
            <div class="form-labels-on-top" >

            
            

            <div class="form-title-row">
                <h1><strong>ENTER DETAILS</strong></h1>
            </div>
            
            <div class="form-row">
                <label>
                    <span>BANK TYPE</span>

                    <select name="dropdown">
                        <option>National Network Bank</option>
                        <option>Continental Network Bank</option>
                        <option>Global Network Bank</option>
                        <option>Operater Bank</option>
                        
                    </select>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>COUNTRY</span>
                    <select name="country">
    <option value=""> -- select one -- </option>                
    
    <option value="AF">Afghanistan</option>
    <option value="AX">Åland Islands</option>
    <option value="AL">Albania</option>
    <option value="DZ">Algeria</option>
    <option value="AS">American Samoa</option>
    <option value="AD">Andorra</option>
    <option value="AO">Angola</option>
    <option value="AI">Anguilla</option>
    <option value="AQ">Antarctica</option>
    <option value="AG">Antigua and Barbuda</option>
    <option value="AR">Argentina</option>
    <option value="AM">Armenia</option>
    <option value="AW">Aruba</option>
    <option value="AU">Australia</option>
    <option value="AT">Austria</option>
    <option value="AZ">Azerbaijan</option>
    <option value="BS">Bahamas</option>
    <option value="BH">Bahrain</option>
    <option value="BD">Bangladesh</option>
    <option value="BB">Barbados</option>
    <option value="BY">Belarus</option>
    <option value="BE">Belgium</option>
    <option value="BZ">Belize</option>
    <option value="BJ">Benin</option>
    <option value="BM">Bermuda</option>
    <option value="BT">Bhutan</option>
    <option value="BO">Bolivia, Plurinational State of</option>
    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
    <option value="BA">Bosnia and Herzegovina</option>
    <option value="BW">Botswana</option>
    <option value="BV">Bouvet Island</option>
    <option value="BR">Brazil</option>
    <option value="IO">British Indian Ocean Territory</option>
    <option value="BN">Brunei Darussalam</option>
    <option value="BG">Bulgaria</option>
    <option value="BF">Burkina Faso</option>
    <option value="BI">Burundi</option>
    <option value="KH">Cambodia</option>
    <option value="CM">Cameroon</option>
    <option value="CA">Canada</option>
    <option value="CV">Cape Verde</option>
    <option value="KY">Cayman Islands</option>
    <option value="CF">Central African Republic</option>
    <option value="TD">Chad</option>
    <option value="CL">Chile</option>
    <option value="CN">China</option>
    <option value="CX">Christmas Island</option>
    <option value="CC">Cocos (Keeling) Islands</option>
    <option value="CO">Colombia</option>
    <option value="KM">Comoros</option>
    <option value="CG">Congo</option>
    <option value="CD">Congo, the Democratic Republic of the</option>
    <option value="CK">Cook Islands</option>
    <option value="CR">Costa Rica</option>
    <option value="CI">Côte d'Ivoire</option>
    <option value="HR">Croatia</option>
    <option value="CU">Cuba</option>
    <option value="CW">Curaçao</option>
    <option value="CY">Cyprus</option>
    <option value="CZ">Czech Republic</option>
    <option value="DK">Denmark</option>
    <option value="DJ">Djibouti</option>
    <option value="DM">Dominica</option>
    <option value="DO">Dominican Republic</option>
    <option value="EC">Ecuador</option>
    <option value="EG">Egypt</option>
    <option value="SV">El Salvador</option>
    <option value="GQ">Equatorial Guinea</option>
    <option value="ER">Eritrea</option>
    <option value="EE">Estonia</option>
    <option value="ET">Ethiopia</option>
    <option value="FK">Falkland Islands (Malvinas)</option>
    <option value="FO">Faroe Islands</option>
    <option value="FJ">Fiji</option>
    <option value="FI">Finland</option>
    <option value="FR">France</option>
    <option value="GF">French Guiana</option>
    <option value="PF">French Polynesia</option>
    <option value="TF">French Southern Territories</option>
    <option value="GA">Gabon</option>
    <option value="GM">Gambia</option>
    <option value="GE">Georgia</option>
    <option value="DE">Germany</option>
    <option value="GH">Ghana</option>
    <option value="GI">Gibraltar</option>
    <option value="GR">Greece</option>
    <option value="GL">Greenland</option>
    <option value="GD">Grenada</option>
    <option value="GP">Guadeloupe</option>
    <option value="GU">Guam</option>
    <option value="GT">Guatemala</option>
    <option value="GG">Guernsey</option>
    <option value="GN">Guinea</option>
    <option value="GW">Guinea-Bissau</option>
    <option value="GY">Guyana</option>
    <option value="HT">Haiti</option>
    <option value="HM">Heard Island and McDonald Islands</option>
    <option value="VA">Holy See (Vatican City State)</option>
    <option value="HN">Honduras</option>
    <option value="HK">Hong Kong</option>
    <option value="HU">Hungary</option>
    <option value="IS">Iceland</option>
    <option value="IN">India</option>
    <option value="ID">Indonesia</option>
    <option value="IR">Iran, Islamic Republic of</option>
    <option value="IQ">Iraq</option>
    <option value="IE">Ireland</option>
    <option value="IM">Isle of Man</option>
    <option value="IL">Israel</option>
    <option value="IT">Italy</option>
    <option value="JM">Jamaica</option>
    <option value="JP">Japan</option>
    <option value="JE">Jersey</option>
    <option value="JO">Jordan</option>
    <option value="KZ">Kazakhstan</option>
    <option value="KE">Kenya</option>
    <option value="KI">Kiribati</option>
    <option value="KP">Korea, Democratic People's Republic of</option>
    <option value="KR">Korea, Republic of</option>
    <option value="KW">Kuwait</option>
    <option value="KG">Kyrgyzstan</option>
    <option value="LA">Lao People's Democratic Republic</option>
    <option value="LV">Latvia</option>
    <option value="LB">Lebanon</option>
    <option value="LS">Lesotho</option>
    <option value="LR">Liberia</option>
    <option value="LY">Libya</option>
    <option value="LI">Liechtenstein</option>
    <option value="LT">Lithuania</option>
    <option value="LU">Luxembourg</option>
    <option value="MO">Macao</option>
    <option value="MK">Macedonia, the former Yugoslav Republic of</option>
    <option value="MG">Madagascar</option>
    <option value="MW">Malawi</option>
    <option value="MY">Malaysia</option>
    <option value="MV">Maldives</option>
    <option value="ML">Mali</option>
    <option value="MT">Malta</option>
    <option value="MH">Marshall Islands</option>
    <option value="MQ">Martinique</option>
    <option value="MR">Mauritania</option>
    <option value="MU">Mauritius</option>
    <option value="YT">Mayotte</option>
    <option value="MX">Mexico</option>
    <option value="FM">Micronesia, Federated States of</option>
    <option value="MD">Moldova, Republic of</option>
    <option value="MC">Monaco</option>
    <option value="MN">Mongolia</option>
    <option value="ME">Montenegro</option>
    <option value="MS">Montserrat</option>
    <option value="MA">Morocco</option>
    <option value="MZ">Mozambique</option>
    <option value="MM">Myanmar</option>
    <option value="NA">Namibia</option>
    <option value="NR">Nauru</option>
    <option value="NP">Nepal</option>
    <option value="NL">Netherlands</option>
    <option value="NC">New Caledonia</option>
    <option value="NZ">New Zealand</option>
    <option value="NI">Nicaragua</option>
    <option value="NE">Niger</option>
    <option value="NG">Nigeria</option>
    <option value="NU">Niue</option>
    <option value="NF">Norfolk Island</option>
    <option value="MP">Northern Mariana Islands</option>
    <option value="NO">Norway</option>
    <option value="OM">Oman</option>
    <option value="PK">Pakistan</option>
    <option value="PW">Palau</option>
    <option value="PS">Palestinian Territory, Occupied</option>
    <option value="PA">Panama</option>
    <option value="PG">Papua New Guinea</option>
    <option value="PY">Paraguay</option>
    <option value="PE">Peru</option>
    <option value="PH">Philippines</option>
    <option value="PN">Pitcairn</option>
    <option value="PL">Poland</option>
    <option value="PT">Portugal</option>
    <option value="PR">Puerto Rico</option>
    <option value="QA">Qatar</option>
    <option value="RE">Réunion</option>
    <option value="RO">Romania</option>
    <option value="RU">Russian Federation</option>
    <option value="RW">Rwanda</option>
    <option value="BL">Saint Barthélemy</option>
    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
    <option value="KN">Saint Kitts and Nevis</option>
    <option value="LC">Saint Lucia</option>
    <option value="MF">Saint Martin (French part)</option>
    <option value="PM">Saint Pierre and Miquelon</option>
    <option value="VC">Saint Vincent and the Grenadines</option>
    <option value="WS">Samoa</option>
    <option value="SM">San Marino</option>
    <option value="ST">Sao Tome and Principe</option>
    <option value="SA">Saudi Arabia</option>
    <option value="SN">Senegal</option>
    <option value="RS">Serbia</option>
    <option value="SC">Seychelles</option>
    <option value="SL">Sierra Leone</option>
    <option value="SG">Singapore</option>
    <option value="SX">Sint Maarten (Dutch part)</option>
    <option value="SK">Slovakia</option>
    <option value="SI">Slovenia</option>
    <option value="SB">Solomon Islands</option>
    <option value="SO">Somalia</option>
    <option value="ZA">South Africa</option>
    <option value="GS">South Georgia and the South Sandwich Islands</option>
    <option value="SS">South Sudan</option>
    <option value="ES">Spain</option>
    <option value="LK">Sri Lanka</option>
    <option value="SD">Sudan</option>
    <option value="SR">Suriname</option>
    <option value="SJ">Svalbard and Jan Mayen</option>
    <option value="SZ">Swaziland</option>
    <option value="SE">Sweden</option>
    <option value="CH">Switzerland</option>
    <option value="SY">Syrian Arab Republic</option>
    <option value="TW">Taiwan, Province of China</option>
    <option value="TJ">Tajikistan</option>
    <option value="TZ">Tanzania, United Republic of</option>
    <option value="TH">Thailand</option>
    <option value="TL">Timor-Leste</option>
    <option value="TG">Togo</option>
    <option value="TK">Tokelau</option>
    <option value="TO">Tonga</option>
    <option value="TT">Trinidad and Tobago</option>
    <option value="TN">Tunisia</option>
    <option value="TR">Turkey</option>
    <option value="TM">Turkmenistan</option>
    <option value="TC">Turks and Caicos Islands</option>
    <option value="TV">Tuvalu</option>
    <option value="UG">Uganda</option>
    <option value="UA">Ukraine</option>
    <option value="AE">United Arab Emirates</option>
    <option value="GB">United Kingdom</option>
    <option value="US">United States</option>
    <option value="UM">United States Minor Outlying Islands</option>
    <option value="UY">Uruguay</option>
    <option value="UZ">Uzbekistan</option>
    <option value="VU">Vanuatu</option>
    <option value="VE">Venezuela, Bolivarian Republic of</option>
    <option value="VN">Viet Nam</option>
    <option value="VG">Virgin Islands, British</option>
    <option value="VI">Virgin Islands, U.S.</option>
    <option value="WF">Wallis and Futuna</option>
    <option value="EH">Western Sahara</option>
    <option value="YE">Yemen</option>
    <option value="ZM">Zambia</option>
    <option value="ZW">Zimbabwe</option>
</select>
                </label>
            </div>
            



            
            



           <div class="form-row">
           <h4 style="font: bold 14px sans-serif;">CURRENCIES</h4>
           <br>

            
            <dl class="dropdown">
            <dt>
            <a href="#">
               <span class="hida">Select currencies</span> 
               <p class="multiSel"></p>


            </a>
            </dt>

            <dd>
            <div class="mutliSelect">
            <ul>
                    <li>
            <input type="checkbox"value="AFN">Afghan afghani</li>
                     <li>
            <input type="checkbox"value="ALL">Albanian lek</li>
                     <li>
            <input type="checkbox"value="DZD">Algerian dinar</li>
                     <li>
            <input type="checkbox"value="AOA">Angolan kwanza</li>
                     <li>
            <input type="checkbox"value="ARS">Argentine peso</li>
                     <li>
            <input type="checkbox"value="AMD">Armenian dram</li>
                     <li>
            <input type="checkbox"value="AWG">Aruban florin</li>
                     <li>
            <input type="checkbox"value="AUD">Australian dollar</li>
                     <li>
            <input type="checkbox"value="AZN">Azerbaijani manat</li>
                     <li>
            <input type="checkbox"value="XCD">Antigua and Barbuda</li>
                     <li>
            <input type="checkbox"value="ARS">Argentina</li>
                     <li>
            <input type="checkbox"value="AMD">Armenia</li>
                     <li>
            <input type="checkbox"value="AWG">Aruba (Netherlands)</li>
                     <li>
            <input type="checkbox"value="SHP">Ascension Island (UK)</li>
                     <li>
            <input type="checkbox"value="AUD">Australia</li>
                     <li>
            <input type="checkbox"value="EUR">Austria</li>
                     <li>
            <input type="checkbox"value="AZN">Azerbaijan</li>
                   </li>
                   <li label="B">
                    <li>
            <input type="checkbox"value="BSD">Bahamian dollar</li>
                    <li>
            <input type="checkbox"value="BHD">Bahraini dinar</li>
                    <li>
            <input type="checkbox"value="BDT">Bangladeshi taka</li>
                    <li>
            <input type="checkbox"value="BBD">Barbadian dollar</li>
                    <li>
            <input type="checkbox"value="BYN">Belarusian ruble</li>
                    <li>
            <input type="checkbox"value="BZD">Belize dollar</li>
                    <li>
            <input type="checkbox"value="BMD">Bermudian dollar</li>
                    <li>
            <input type="checkbox"value="BTN">Bhutanese ngultrum</li>
                    <li>
            <input type="checkbox"value="BOB">Bolivian boliviano</li>
                    <li>
            <input type="checkbox"value="BAM">Bosnia and Herzegovina convertible mark</li>
                    <li>
            <input type="checkbox"value="BWP">Botswana pula</li>
                    <li>
            <input type="checkbox"value="BRL">Brazilian real</li>
                    <li>
            <input type="checkbox"value="BND">Brunei dollar</li>
                    <li>
            <input type="checkbox"value="BGN">Bulgarian lev</li>
                    <li>
            <input type="checkbox"value="BIF">Burundi franc</li>
                   </li>
                   <li label="C">
                    <li>
            <input type="checkbox"value="CVE">Cape Verdean escudo</li>
                    <li>
            <input type="checkbox"value="KHR">Cambodian riel</li>
                    <li>
            <input type="checkbox"value="XAF">Central African CFA franc</li>
                    <li>
            <input type="checkbox"value="CAD">Canadian dollar</li>
                    <li>
            <input type="checkbox"value="KYD">Cayman Islands dollar</li>
                    <li>
            <input type="checkbox"value="XAF">Central African CFA franc</li>
                    <li>
            <input type="checkbox"value="CLP">Chilean peso</li>
                    <li>
            <input type="checkbox"value="CNY">Chinese Yuan Renminbi</li>
                    <li>
            <input type="checkbox"value="COP">Colombian peso</li>
                    <li>
            <input type="checkbox"value="KMF">Comorian franc</li>
                    <li>
            <input type="checkbox"value="CDF">Congolese franc</li>
                    <li>
            <input type="checkbox"value="none">Cook Islands dollar</li>
                    <li>
            <input type="checkbox"value="CRC">Costa Rican colon</li>
                    <li>
            <input type="checkbox"value="HRK">Croatian kuna</li>
                    <li>
            <input type="checkbox"value="CUP">Cuban peso</li>
                    <li>
            <input type="checkbox"value="CZK">Czech koruna</li>
                    <li>
            <input type="checkbox"value="XPF">CFP franc</li>
                   </li>
                   <li label="D">
                    <li>
            <input type="checkbox"value="DKK">Danish krone</li>
                    <li>
            <input type="checkbox"value="DJF">Djiboutian franc</li>
                    <li>
            <input type="checkbox"value="DOP">Dominican peso</li>
                    <li>
            <input type="checkbox"value="DKK">Danish krone</li>
                   </li>
                   <li label="E">
                    <li>
            <input type="checkbox"value="EUR">European euro</li>
                    <li>
            <input type="checkbox"value="XCD">East Caribbean dollar</li>
                    <li>
            <input type="checkbox"value="EGP">Egyptian pound</li>
                    <li>
            <input type="checkbox"value="ERN">Eritrean nakfa</li>
                    <li>
            <input type="checkbox"value="ETB">Ethiopian birr</li>
                   </li>
                   <li label="F">
                    <li>
            <input type="checkbox"value="FKP">Falkland Islands pound</li>
                    <li>
            <input type="checkbox"value="none">Faroese krona</li>
                    <li>
            <input type="checkbox"value="FJD">Fijian dollar</li>
                   </li>
                   <li label="G">
                    <li>
            <input type="checkbox"value="GMD">Gambian dalasi</li>
                    <li>
            <input type="checkbox"value="GEL">Georgian lari</li>
                    <li>
            <input type="checkbox"value="GHS">Ghanaian cedi</li>
                    <li>
            <input type="checkbox"value="GIP">Gibraltar pound</li>
                    <li>
            <input type="checkbox"value="GTQ">Guatemalan quetzal</li>
                    <li>
            <input type="checkbox"value="GGP">Guernsey Pound</li>
                    <li>
            <input type="checkbox"value="GNF">Guinean franc</li>
                    <li>
            <input type="checkbox"value="GYD">Guyanese dollar</li>
                   </li>
                   <li label="H">
                    <li>
            <input type="checkbox"value="HTG">Haitian gourde</li>
                    <li>
            <input type="checkbox"value="HNL">Honduran lempira</li>
                    <li>
            <input type="checkbox"value="HKD">Hong Kong dollar</li>
                    <li>
            <input type="checkbox"value="HUF">Hungarian forint</li>
                   </li>
                   <li label="I">
                    <li>
            <input type="checkbox"value="ISK">Icelandic krona</li>
                    <li>
            <input type="checkbox"value="INR">Indian rupee </li>
                    <li>
            <input type="checkbox"value="IDR">Indonesian rupiah</li>
                    <li>
            <input type="checkbox"value="ILS">Israeli new shekel</li>
                    <li>
            <input type="checkbox"value="IRR">Iranian rial</li>
                    <li>
            <input type="checkbox"value="IQD">Iraqi dinar</li>
                   </li>
                   <li label="J">
                    <li>
            <input type="checkbox"value="JMD">Jamaican dollar</li>
                    <li>
            <input type="checkbox"value="JPY">Japanese yen</li>
                    <li>
            <input type="checkbox"value="JEP">Jersey pound</li>
                    <li>
            <input type="checkbox"value="JOD">Jordanian dinar</li>
                   </li>
                   <li label="K">
                    <li>
            <input type="checkbox"value="KZT">Kazakhstani tenge</li>
                    <li>
            <input type="checkbox"value="KES">Kenyan shilling</li>
                    <li>
            <input type="checkbox"value="KWD">Kuwaiti dinar</li>
                    <li>
            <input type="checkbox"value="KGS">Kyrgyzstani som</li>
                   </li>
                   <li label="L">
                    <li>
            <input type="checkbox"value="LAK">Lao kip</li>
                    <li>
            <input type="checkbox"value="LBP">Lebanese pound</li>
                    <li>
            <input type="checkbox"value="LSL">Lesotho loti</li>
                    <li>
            <input type="checkbox"value="LRD">Liberian dollar</li>
                    <li>
            <input type="checkbox"value="LYD">Libyan dinar</li>
                   </li>
                   <li label="M">
                    <li>
            <input type="checkbox"value="IMP">Manx pound </li>
                    <li>
            <input type="checkbox"value="MOP">Macanese pataca</li>
                    <li>
            <input type="checkbox"value="MKD">Macedonian denar</li>
                    <li>
            <input type="checkbox"value="MGA">Malagasy ariary</li>
                    <li>
            <input type="checkbox"value="MWK">Malawian kwacha</li>
                    <li>
            <input type="checkbox"value="MYR">Malaysian ringgit</li>
                    <li>
            <input type="checkbox"value="MVR">Maldivian rufiyaa</li>
                    <li>
            <input type="checkbox"value="MRO">Mauritanian ouguiya</li>
                    <li>
            <input type="checkbox"value="MUR">Mauritian rupee</li>
                    <li>
            <input type="checkbox"value="MXN">Mexican peso</li>
                    <li>
            <input type="checkbox"value="MDL">Moldovan leu</li>
                    <li>
            <input type="checkbox"value="MNT">Mongolian tugrik</li>
                    <li>
            <input type="checkbox"value="MAD">Moroccan dirham</li>
                    <li>
            <input type="checkbox"value="MZN">Mozambican metical</li>
                    <li>
            <input type="checkbox"value="MMK">Myanmar kyat</li>
                   </li>
                   <li label="N">
                    <li>
            <input type="checkbox"value="NZD">New Zealand dollar</li>
                    <li>
            <input type="checkbox"value="ANG">Netherlands Antillean guilder</li>
                    <li>
            <input type="checkbox"value="NAD">Namibian dollar</li>
                    <li>
            <input type="checkbox"value="NPR">Nepalese rupee</li>
                    <li>
            <input type="checkbox"value="TWD">New Taiwan dollar</li>
                    <li>
            <input type="checkbox"value="NIO">Nicaraguan cordoba</li>
                    <li>
            <input type="checkbox"value="NGN">Nigerian naira</li>
                    <li>
            <input type="checkbox"value="KPW">North Korean won</li>
                    <li>
            <input type="checkbox"value="NOK">Norwegian krone</li>
                   </li>
                   <li label="O">
                    <li>
            <input type="checkbox"value="OMR">Omani rial</li>
                   </li>
                   <li label="P">
                    <li>
            <input type="checkbox"value="PKR">Pakistani rupee</li>
                    <li>
            <input type="checkbox"value="PGK">Papua New Guinean kina</li>
                    <li>
            <input type="checkbox"value="PYG">Paraguayan guarani</li>
                    <li>
            <input type="checkbox"value="PEN">Peruvian sol</li>
                    <li>
            <input type="checkbox"value="PHP">Philippine peso</li>
                    <li>
            <input type="checkbox"value="PLN">Polish zloty</li>
                    <li>
            <input type="checkbox"value="GBP">Pound sterling</li>
                   </li>
                   <li label="Q">
                    <li>
            <input type="checkbox"value="QAR">Qatari riyal</li>
                   </li>
                   <li label="R">
                    <li>
            <input type="checkbox"value="RON">Romanian leu</li>
                    <li>
            <input type="checkbox"value="RUB">Russian ruble</li>
                    <li>
            <input type="checkbox"value="RWF">Rwandan franc</li>
                   </li>
                   <li label="S">
                    <li>
            <input type="checkbox"value="SHP">Saint Helena pound</li>
                    <li>
            <input type="checkbox"value="XDR">SDR (Special Drawing Right)</li>
                    <li>
            <input type="checkbox"value="WST">Samoan tala</li>
                    <li>
            <input type="checkbox"value="STD">Sao Tome and Principe dobra</li>
                    <li>
            <input type="checkbox"value="SAR">Saudi Arabian riyal</li>
                    <li>
            <input type="checkbox"value="RSD">Serbian dinar</li>
                    <li>
            <input type="checkbox"value="SCR">Seychellois rupee</li>
                    <li>
            <input type="checkbox"value="SLL">Sierra Leonean leone</li>
                    <li>
            <input type="checkbox"value="SGD">Singapore dollar</li>
                    <li>
            <input type="checkbox"value="SBD">Solomon Islands dollar</li>
                    <li>
            <input type="checkbox"value="SOS">Somali shilling</li>
                    <li>
            <input type="checkbox"value="ZAR">South African rand</li>
                    <li>
            <input type="checkbox"value="KRW">South Korean won</li>
                    <li>
            <input type="checkbox"value="SSP">South Sudanese pound</li>
                    <li>
            <input type="checkbox"value="LKR">Sri Lankan rupee</li>
                    <li>
            <input type="checkbox"value="SDG">Sudanese pound</li>
                    <li>
            <input type="checkbox"value="SRD">Surinamese dollar</li>
                    <li>
            <input type="checkbox"value="SZL">Swazi lilangeni</li>
                    <li>
            <input type="checkbox"value="SEK">Swedish krona</li>
                    <li>
            <input type="checkbox"value="CHF">Swiss franc</li>
                    <li>
            <input type="checkbox"value="SYP">Syrian pound</li>
                   </li>
                   <li label="T">
                    <li>
            <input type="checkbox"value="TJS">Tajikistani somoni</li>
                    <li>
            <input type="checkbox"value="TZS">Tanzanian shilling</li>
                    <li>
            <input type="checkbox"value="THB">Thai baht</li>
                    <li>
            <input type="checkbox"value="TOP">Tongan pa’anga</li>
                    <li>
            <input type="checkbox"value="TTD">Trinidad and Tobago dollar</li>
                    <li>
            <input type="checkbox"value="TND">Tunisian dinar</li>
                    <li>
            <input type="checkbox"value="TRY">Turkish lira</li>
                    <li>
            <input type="checkbox"value="TMT">Turkmen manat</li>
                   </li>
                   <li label="U">
                    <li>
            <input type="checkbox"value="USD">United States dollar</li>
                    <li>
            <input type="checkbox"value="UGX">Ugandan shilling</li>
                    <li>
            <input type="checkbox"value="UAH">Ukrainian hryvnia</li>
                    <li>
            <input type="checkbox"value="AED">UAE dirham</li>
                    <li>
            <input type="checkbox"value="UYU">Uruguayan peso</li>
                    <li>
            <input type="checkbox"value="UZS">Uzbekistani som</li>
                   </li>
                   <li label="V">
                    <li>
            <input type="checkbox"value="VUV">Vanuatu vatu</li>
                    <li>
            <input type="checkbox"value="VEF">Venezuelan bolivar</li>
                    <li>
            <input type="checkbox"value="VND">Vietnamese dong</li>
                   </li>
                   <li label="W">
                    <li>
            <input type="checkbox"value="XOF">West African CFA franc</li>
                   </li>
                   <li label="Y">
                    <li>
            <input type="checkbox"value="YER">Yemeni rial</li>
                   </li>
                   <li label="Z">
                    <li>
            <input type="checkbox"value="ZMW">Zambian kwacha</li>
                  </ul>

                



            </div>
                


            </dd>
 </dl>
 
            </div>

            
            
             
<br> <br> <br> <br> <br> <br> <br>
            
            <div class="form-row">
                <label>
       
                    <span>BANK NAME</span>
                    <input type="text" name="name" placeholder="" class="validate">
                </label>
            </div>
            <div class="form-row">
                <label>
       
                    <span>PASSWORD</span>
                    <input type="password" name="name" placeholder="" class="validate">
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>E-MAIL ADDRESS</span>
                    <input type="email" name="email" placeholder="">
                </label>
            </div>
            <div class="form-row">
                <label>
       
                    <span>MANAGEMENT LOCK</span>
                    <input type="password" name="name" placeholder="" class="validate">
                </label>
            </div>
            <div class="form-row">
                <label>
       
                    <span>MAINS CODE</span>
                    <input type="password" name="name" placeholder="" class="validate">
                </label>
            </div>
            <div class="form-row">
                <label>
       
                    <span>ENGINE CODE</span>
                    <input type="password" name="name" placeholder="" class="validate">
                </label>
            </div>

            
            
        

            
            
            
            </div>
            
            </div>


            <div class="panel-footer">
             
            <div class="container">
    
    <div class="button pull-right">
      
      
      
      
      
      <button type="submit" class="btn btn-sm btn-primary"> DONE </button>
      
          </form>
          
          </div>
      </div>

      </div>



        </div>
        </div>
        </div>




            




</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>
    <script  src="assets/js/dropdown.js"></script>

    

</html>
