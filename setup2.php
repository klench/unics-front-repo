<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>unics dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- <link href="style.css" rel="stylesheet" /> -->
    <link href="login.css" rel="stylesheet" />
    <link href="createaccount1.css" rel="stylesheet" />
    


</head>
<style>
        
        body{
          padding-top: 80px;
          padding-bottom: 20px;  
            
        }
    
    
    
    </style>


<body>

        <div class="container-fluid">
        <div class="panel-panel default">
        <div class="panel body text-center" >
         <div class="container" >
         <h2 style="font-style: italic;"><strong>CONGRATULATIONS!!!</strong></h2>
         


        <h4 style="font-style: italic;">Your Bank Setup has been completed <br>successfully.<br>
            Your Vault ID is PYG6758.
            Please log in to your <a href="enginesignin.php">
                   
                ENGINE</a> to add branch and manager to complete setup. <br> Make sure you write down your ID else you may loose it when you skip this page Thankyou.</h4>
         
         </div>



         <br> <br> <br> <br> <br> <br> <br>  <br> <br> <br>
        </div>
        
</div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

</html>
