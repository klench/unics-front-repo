<!doctype html>
<html lang="en">
<head>
<title>unics dashboard</title>
    <!-- Bootstrap core CSS     -->
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="style.css" rel="stylesheet" />
    <link href="login.css" rel="stylesheet" />
    <link href="createaccount1.css" rel="stylesheet" />

                                
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    


</head>

<body><!-- the wrapper consist of the overall body -->

<div class="wrapper">
    <div class="sidebar" data-color="black" data-image="assets/img/unicssidebar.png">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

        <!-- sidebar element are written here -->
<div class="sidebar-wrapper" style="padding-top: 60PX;">
                    <div class="logo">
                         <a href="createaccount1.php" class="simple-text"> <img src="assets\img\controller2.png">
                         </a>

                    </div>


            <ul class="nav">
                
                
                
                <li class="active">
                    <a href="createaccount3.php">
                        
                        <i> <img src="assets\img\addmanager.png"></i>
                        <p>OPEN ACCOUNT</p>
                    </a>
                </li>
                <li>
                    <a href="viewaccount.php">
                        
                        <i> <img src="assets\img\viewteller.png"></i>
                        <p>VIEW ACCOUNT</p>
                    </a>
                </li>
                <li>
                    <a href="settle.php">
                        
                        <i> <img src="assets\img\settlements.png"></i>
                        <p>SETTLE</p>
                    </a>
                </li>

                <li>
                    <a href="withdraw.php">
                        
                        <i> <img src="assets\img\withdrawals.png"></i>
                        <p>WITHDRAW</p>
                    </a>
                </li>
                <li>
                    <a href="checkbalance.php">
                        
                        <i> <img src="assets\img\checkbalance.png"></i>
                        <p>CHECK BALANCE</p>
                    </a>
                </li>
                <li>
                    <a href="deposit.php">
                        
                        <i> <img src="assets\img\deposits.png"></i>
                        <p>DEPOSIT</p>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </div>

    <!-- the main-panel consist of the rest of the colomn after the sidebar and it is in the wrapper  -->
<div class="main-panel">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                          <div class="logo"><div class="navbar-brand"><img src="assets\img\UNICS.png"  style="margin-top: -7px;">
                           </div>
                           </div>
                            
                        </div>
                <div class="collapse navbar-collapse" >
                    
                        <ul class="nav navbar-nav  ">
                        <li class="active">
                           <a href="createaccount1.php "  >
                               <i> <img src="assets\img\navcontroller.png"></i>  <strong>CONTROLLER</strong>
                            </a>
                        </li>
                        
                        <li >
                            <a href="incoming.php">
                                <i> <img src="assets\img\navcapture.png"></i> <strong>CAPTURE</strong>
                            </a>
                        </li>
                        
                        <li>
                            <form method="post" action="logout.php" id="">
                            <div class="" style="margin-left:280px; padding-top:18px;"  > 
                                    <i style="color: white;">ECOBANK </i>
                                <a href="index.php" title="logout">
                                <img src="assets\img\logouticon.png"></a>
                                    </div>
                                </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>



        <!-- the content consist of the panel which is the panel-head, panel-body and panel-footer -->
    

                    <div class="content" >
                    <div class="container-fluid" style="padding-top: 40px;" >
            <!-- You only need this form and the form-labels-on-top.css -->
            <div class="panel panel-default">
            <form method="post" action="createaccount3.php" id="">
           
              <div class="panel-heading">
              <div class="panel-heading text-center">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1"><h5><strong>ACCOUNT DETAILS </strong></h5></marquee> 
              </div>
              </div>

            <div class="panel-body">

            <div class="row">
             

            <div class="col-lg-6">
            <div class="form-labels-on-top">
            <div class="form-row">
                <label>
                    <span>CEDIS</span>
                    <input type="number" placeholder="&#8373; 0.00" min="0.00"  step="any" value="" name="currency" id="accountcreating" required="required">
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>DOLLAR</span>
                    <input type="number" placeholder="$ 0.00" min="0.00"  step="any" value="" name="currency" id="accountcreating"  required="required">
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>POUND</span>
                    <input type="number" placeholder="&pound; 0.00" min="0.00" step="any" value="" name="currency" id="accountcreating"  required="required">
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>EURO</span>
                    <input type="number" placeholder="&euro; 0.00" min="0.00" step="any" value="" name="currency" id="accountcreating"  required="required">
                </label>
            </div>



             </div>
            </div>
            <div class="col-lg-6">
            <div class="form-labels-on-top">
            <div class="form-row">
                <label>
                    <span>DEFAULT RATE</span>
                    <input type="number" placeholder="0.00" min="0.00" step="any" value="" name="currency" id="accountcreating"  required="required">
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>COLATERAL AMOUNT</span>
                    <input type="number" placeholder="0.00" min="0.00" step="any" value="" name="currency" id="accountcreating"  required="required">
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>CHARGE PERIOD</span>
                    <input type="date" placeholder="dd/mm/yy" min="0.00" step="any" value="" name="currency" id="accountcreating"  required="required">
                </label>
            </div>
            




        </div>
            </div>


     </div>
            
            

                
                </div>

            <div class="panel-footer"> 
            <div class="container">
            
    
    <div class="button pull-right">
<button type="submit" class="btn btn-sm btn-primary">DONE</button>
</div>
</form>

<div class="button pull-left">
<a href="createaccount2.php"> <button type="submit" class="btn btn-sm btn-primary"><< Prev</button></a>
</div>
      </div>
      </div> 

                
        
        </div>
        </div>
        

        
            
                    

                    
            
            


            
            


    
    




</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    

</html>
