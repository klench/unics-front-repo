<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>unics dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link href="style.css" rel="stylesheet" />
    <link href="login.css" rel="stylesheet" />
    <link href="createaccount1.css" rel="stylesheet" />

                                
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    

</head>

<body><!-- the wrapper consist of the overall body -->

<div class="wrapper">
    

    <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                          <div class="logo"><div class="navbar-brand"><img src="assets\img\UNICS.png"  style="margin-top: -7px;">
                           </div>
                           </div>
                    
                </div>
                <div class="collapse navbar-collapse">
                    

                   <ul class="nav navbar-nav ">
                        <li class="active">
                           <a href="transactionslogin.php "  >
                               <i> <img src="assets\img\navtransactions.png"> </i> <strong>TRANSACTIONS</strong>
                            </a>
                        </li>
                        
                        <li >
                            <a href="newaccount1.php">
                                <i> <img src="assets\img\navcontrol.png"> </i> <strong >CONTROL ROOM</strong>
                            </a>
                        </li>
                        <li>
                            <form method="post" action="logout.php" id="">
                            <div class="" style="margin-left:280px; padding-top:18px;"  > 
                                    <i style="color: white;">ECOBANK </i>
                                <a href="index.php" title="logout">
                                <img src="assets\img\logouticon.png"></a>
                                    </div>
                                </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <!-- the content consist of the panel which is the panel-head, panel-body and panel-footer -->
    

                    <div class="content" >
                    <div class="container-fluid" style="padding-top: 70px;" >
            <!-- You only need this form and the form-labels-on-top.css -->
            <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-heading text-center">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1"><h5><strong>TRANSACTIONS LOGIN</strong></h5></marquee> 
              </div>
              </div>
            
            <form method="post" action="settlement.php" id="">

            
            
              
            <div class="panel-body">
            <div class="form-labels-on-top" >

            
            

            <div class="form-title-row">
                <h1><strong>ENTER DETAILS</strong></h1>
            </div>
            
            <div class="form-row">
                <label>

                    <span>BRANCH ID</span>
                    <input type="password"name="name" placeholder="" class="validate" required="required">
                </label>
            </div>
            <div class="form-row">
                <label>

                    <span>TELLER ID</span>
                    <input type="password"name="name" placeholder="" class="validate" required="required">
                </label>
            </div>
            
            
        

            
            
            
            </div>
            </div>

            <div class="panel-footer">
             
            <div class="container">
    
    <div class="button pull-right">
      
      
      
      
      
      <button type="submit" class="btn btn-sm btn-primary">DONE</button>
      
          
          
          </div>
      </div>

      </div>


</form>
        </div>
        </div>
        </div>


            




</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    

</html>
