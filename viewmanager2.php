<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>unics dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="style.css" rel="stylesheet" />
    <link href="login.css" rel="stylesheet" />
    <link href="createaccount1.css" rel="stylesheet" />
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    


</head>
<body><!-- the wrapper consist of the overall body -->

<div class="wrapper">
    <div class="sidebar" data-color="black" data-image="assets/img/unicssidebar.png">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

        <!-- sidebar element are written here -->
<div class="sidebar-wrapper" style="padding-top: 60PX;">
                    <div class="logo">
                         <a href="newaccount1.php" class="simple-text"> <img src="assets\img\control2.png" >
                           
                        </a>
                    </div>


            <ul class="nav">
                <li>
                    <a href="newaccount1.php">
                        
                        <i> <img src="assets\img\accounts.png"></i>
                        <p>ACCOUNTS</p>
                    </a>
                </li>
                <li>
                    <a href="changelock.php">
                        <i> <img src="assets\img\changelock.png"></i>
                        
                        <p>CHANGE LOCK</p>
                    </a>
                </li>
                <li>
                    <a href="addbranch.php">
                        <i> <img src="assets\img\addbranch.png"></i>
                        
                        <p>ADD BRANCH</p>
                    </a>
                </li>
                <li>
                    <a href="removebranch.php">
                        
                        <i> <img src="assets\img\removebranch.png"></i>
                        <p>REMOVE BRANCH</p>
                    </a>
                </li>
                <li>
                    <a href="ctrcontrol.php">
                        
                        <i> <img src="assets\img\ctrcontrol.png"></i>
                        <p>CTR CONTROLS</p>
                    </a>
                </li>
                <li>
                    <a href="addteller.php">
                        
                        <i> <img src="assets\img\addteller.png"></i>
                        <p>ADD TELLER</p>
                    </a>
                </li>

                <li >
                    <a href="viewteller.php">
                        
                        <i> <img src="assets\img\viewteller.png"></i>
                        <p>VIEW TELLER</p>
                    </a>
                </li>
                 <li >
                    <a href="addmanager.php">
                        
                        <i> <img src="assets\img\addmanager.png"></i>
                        <p>ADD MANAGER</p>
                    </a>
                </li>
                <li class="active">
                    <a href="viewmanager2.php">
                        
                        <i> <img src="assets\img\viewmanager.png"></i>
                        <p>VIEW MANAGER</p>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </div>

    <!-- the main-panel consist of the rest of the colomn after the sidebar and it is in the wrapper  -->
<div class="main-panel">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                          <div class="logo"><div class="navbar-brand"><img src="assets\img\UNICS.png"  style="margin-top: -7px;">
                           </div>
                           </div>
                            
                        </div>
                <div class="collapse navbar-collapse" >
                    
                        <ul class="nav navbar-nav">
                        <li>
                           <a href="transactionslogin.php"  >
                               <i> <img src="assets\img\navtransactions.png"> </i> <strong>TRANSACTIONS</strong>
                            </a>
                        </li>
                        
                        <li class="active">
                            <a href="newaccount1.php">
                                <i> <img src="assets\img\navcontrol.png"> </i> <strong >CONTROL ROOM</strong>
                            </a>
                        </li>
                        <li>
                            <form method="post" action="logout.php" id="">
                            <div class="" style="margin-left:280px; padding-top:18px;"  > 
                                    <i style="color: white;">ECOBANK </i>
                                <a href="index.php" title="logout">
                                <img src="assets\img\logouticon.png"></a>
                                    </div>
                                </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- the content consist of the panel which is the panel-head, panel-body and panel-footer -->
    

                    <div class="content" >
                    <div class="container-fluid" style="padding-top: 40px;" >
            <!-- You only need this form and the form-labels-on-top.css -->
            <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-heading text-center">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1"><h5><strong>MANAGER DETAILS</strong></h5></marquee> 
              </div>
              </div>
            
            <form method="post" action="addmanager2.php" id="">

            
            
              
           <div class="panel-body">
            
            
            
           <div class="row">
            <div class="col-lg-4">
            <!-- <div class="form-labels-on-top" > -->
            
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>FIRST NAME</strong></h5>
                    <p>KING</p>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>MIDDLE NAME</strong></h5>
                    <p>KOFI</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>LAST NAME</strong></h5>
                    <p>SAFO</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>GENDER</strong></h5>
                    <p>MALE</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>MARITAL STATUS</strong></h5>
                    <p>SINGLE</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>E-MAIL</strong></h5>
                    <p>KING.SAFO@AT.GIMPA.EDU.GH</p>
                </label>
            </div>



             </div>
            
            <div class="col-lg-4">
            <!-- <div class="form-labels-on-top" > -->
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>DATE OF BIRTH/COMMENCEMENT</strong></h5>
                    <p>19/APRIL/1994</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>COUNTRY OF RESIDENCE</strong></h5>
                    <p>GHANA</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>NATIONALITY</strong></h5>
                    <p>GHANAIAN</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>REGION/STATE/PROVINCE</strong></h5>
                    <p>GREATER ACCRA</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>CITY/TOWN/VILLAGE</strong></h5>
                    <p>ACCRA</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>HOME ADDRESS</strong></h5>
                    <p>PLT 53 BLK 21</p>
                </label>
            </div>
            
            




        </div>

         <div class="col-lg-4">
            <!-- <div class="form-labels-on-top" > -->
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>PREMISES NUMBER</strong></h5>
                    <p>111666</p>
                </label>
            </div>
            
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>PHONE NUMBER</strong></h5>
                    <p>+233 540 924 700</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>ID CARD NUMBER</strong></h5>
                    <p>215025679</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>TIN NUMBER</strong></h5>
                    <p>P000154875</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>ACCOUNT NUMBER</strong></h5>
                    <p>2030015438910</p>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <h5 style="color: blue;"><strong>SIGNATORY PHONE NUMBER</strong></h5>
                    <p>+233 244 154 436</p>
                </label>
            </div>
            
            




        </div>
            </div>
            </div>

</form>
        </div>
        </div>
        </div>
        </div>
        </div>


       

</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    

</html>
